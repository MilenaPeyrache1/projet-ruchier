

#include <em_common.h>
#include "sl_btmesh.h"
#include "sl_btmesh_event_log.h"
#include "sl_btmesh_provisioning_decorator.h"
#include "sl_btmesh_sensor_server.h"

void sl_btmesh_init(void)
{
  sl_btmesh_class_health_server_init();
  sl_btmesh_class_proxy_init();
  sl_btmesh_class_proxy_server_init();
  sl_btmesh_class_sensor_server_init();
  sl_btmesh_class_sensor_setup_server_init();
  sl_btmesh_class_node_init();
}

SL_WEAK void sl_btmesh_on_event(sl_btmesh_msg_t* evt)
{
  (void)(evt);
}

void sl_btmesh_process_event(sl_btmesh_msg_t *evt)
{
  sl_btmesh_handle_btmesh_logging_events(evt);
  sl_btmesh_handle_provisioning_decorator_event(evt);
  sl_btmesh_handle_sensor_server_events(evt);
  sl_btmesh_on_event(evt);
}

SL_WEAK bool sl_btmesh_can_process_event(uint32_t len)
{
  (void)(len);
  return true;
}

void sl_btmesh_step(void)
{
  sl_btmesh_msg_t evt;

  // check if application can process a new event.
  if (!sl_btmesh_can_process_event(SL_BGAPI_MSG_HEADER_LEN + SL_BGAPI_MAX_PAYLOAD_SIZE)) {
    return;
  }

  // Pop (non-blocking) a Bluetooth stack event from event queue.
  sl_status_t status = sl_btmesh_pop_event(&evt);
  if(status != SL_STATUS_OK){
    return;
  }
  sl_btmesh_process_event(&evt);
}
