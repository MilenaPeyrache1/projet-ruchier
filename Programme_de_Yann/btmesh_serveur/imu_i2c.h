/*
 * imu_i2c.h
 *
 *  Created on: 9 janv. 2021
 *      Author: yann le gall
 */

#ifndef IMU_I2C_H_
#define IMU_I2C_H_


#include <stdio.h>
#include <stddef.h>
#include "sl_i2cspm.h"
#include "sl_sleeptimer.h"
#include "stddef.h"
#include "sl_i2cspm_instances.h"


/** Si70xx Read RH Command */
#define SELF_TEST_X_GYRO   0x00
#define MPU_ADDRESS        0x68
#define ACCEL_XOUT_H       0x3B

sl_status_t mpu_get(int16_t *mpu);
sl_status_t measure_mpu(sl_i2cspm_t *i2cspm, uint8_t addr, int16_t *data);


#endif /* IMU_I2C_H_ */
