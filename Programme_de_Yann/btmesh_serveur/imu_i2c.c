/*
 * imu_i2c.c
 *
 *  Created on: 9 janv. 2021
 *      Author: yann le gall
 */

#include "imu_i2c.h"

static sl_status_t mpu_send_command(sl_i2cspm_t *i2cspm, uint8_t addr, int16_t *data,
                                    uint8_t command);



sl_status_t mpu_get(int16_t *mpu)
{
  sl_status_t sc;
  sl_i2cspm_t *mpu_sensor = sl_i2cspm_sensor;
  sc = measure_mpu(mpu_sensor, MPU_ADDRESS, mpu);
  return sc;
}

sl_status_t measure_mpu(sl_i2cspm_t *i2cspm, uint8_t addr, int16_t *data)
{
  sl_status_t retval;

  retval = mpu_send_command(i2cspm, addr, data, ACCEL_XOUT_H);

  if (retval != SL_STATUS_OK) {
    return retval;
  }

  return retval;
}


static sl_status_t mpu_send_command(sl_i2cspm_t *i2cspm, uint8_t addr, int16_t *data,
                                          uint8_t command)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t                    i2c_read_data[6];
  uint8_t                    i2c_write_data[1];

  seq.addr  = addr << 1;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select command to issue */
  i2c_write_data[0] = command;
  seq.buf[0].data   = i2c_write_data;
  seq.buf[0].len    = 1;
  /* Select location/length of data to be read */
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 6;

  ret = I2CSPM_Transfer(i2cspm, &seq);

  if (ret != i2cTransferDone) {
    *data = 0;
    return SL_STATUS_TRANSMIT;
  }
  data[0] = ((int16_t)i2c_read_data[0] << 8) | i2c_read_data[1] ;
  data[1] = ((int16_t)i2c_read_data[2] << 8) | i2c_read_data[3] ;
  data[2] = ((int16_t)i2c_read_data[4] << 8) | i2c_read_data[5] ;

  return SL_STATUS_OK;
}


