#ifndef SL_SENSOR_RHT_CONFIG_H
#define SL_SENSOR_RHT_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Bluetooth Mesh - Temperature Sensor

// <o SENSOR_THERMOMETER_POSITIVE_TOLERANCE> Positive tolerance of sensor.
// <0-4095:1>
// <i> Default: 0 (Unspecified)
// <i> 12-bit Positive Tolerance value (1 - 4095) or Unspecified (0). The value is derived as ERR_P [%] = 100 [%] * x / 4095
#define SENSOR_THERMOMETER_POSITIVE_TOLERANCE 0

// <o SENSOR_THERMOMETER_NEGATIVE_TOLERANCE> Negative tolerance of sensor.
// <0-4095:1>
// <i> Default: 0 (Unspecified)
// <i> 12-bit Negative Tolerance value (1 - 4095) or Unspecified (0). The value is derived as ERR_N [%] = 100 [%] * x / 4095
#define SENSOR_THERMOMETER_NEGATIVE_TOLERANCE 0

// <o SENSOR_THERMOMETER_SAMPLING_FUNCTION> Sampling function
// <SAMPLING_UNSPECIFIED=> Unspecified
// <SAMPLING_INSTANTANEOUS=> Instantaneous sampling
// <SAMPLING_ARITHMETIC_MEAN=> Arithmetic mean
// <SAMPLING_RMS=> Root mean square
// <SAMPLING_ARITHMETIC_MEAN=> Poll
// <SAMPLING_MAXIMUM=> Maximum value
// <SAMPLING_MINIMUM=> Minimum value
// <SAMPLING_ACCUMULATED=> Cumulative moving average updated with the frequency given by Sensor Update Interval
// <SAMPLING_COUNT=> Number of "events" over the period of time defined by the Measurement Period
// <SAMPLING_RFU=> Reserved for Future Use
// <i> Default: Unspecified
#define SENSOR_THERMOMETER_SAMPLING_FUNCTION       SAMPLING_UNSPECIFIED

// <o SENSOR_THERMOMETER_MEASUREMENT_PERIOD> Measurement Period of sensor.
// <0-255:1>
// <i> Default: 0 (Not Applicable)
// <i> 8 bit value (1 - 255) or Not Applicable (0). Time period in seconds is derived as T [s] = 1.1 ^ (x - 64)
#define SENSOR_THERMOMETER_MEASUREMENT_PERIOD 0

// <o SENSOR_THERMOMETER_UPDATE_INTERVAL> Update Interval of sensor.
// <0-255:1>
// <i> Default: 0 (Not Applicable)
// <i> 8 bit value (1-255) or Not Applicable (0). Update interval in seconds is derived as I [s] = 1.1 ^ (x - 64)
#define SENSOR_THERMOMETER_UPDATE_INTERVAL 0

// </h>

// <<< end of configuration section >>>

#endif // SL_SENSOR_RHT_CONFIG_H
