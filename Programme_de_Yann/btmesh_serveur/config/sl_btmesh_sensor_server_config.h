#ifndef SL_BTMESH_SENSOR_SERVER_CONFIG_H
#define SL_BTMESH_SENSOR_SERVER_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Sensor Server configuration

// <e SENSOR_SERVER_LOGGING> Enable Logging
// <i> Default: 1
// <i> Enable / disable UART Logging for Sensor Server model specific messages for this component.
#define SENSOR_SERVER_LOGGING   (1)

// <s.128 SENSOR_SERVER_PEOPLE_COUNT_LOG_TEXT> Log text for People Count.
// <i> Log text for People Count
#define SENSOR_SERVER_PEOPLE_COUNT_LOG_TEXT "People_count: %u\r\n"

// <s.128 SENSOR_SERVER_TEMPERATURE_LOG_TEXT> Log text for Temperature.
// <i> Log text for Temperature
#define SENSOR_SERVER_TEMPERATURE_LOG_TEXT "Temperature: %d\r\n"

// <s.128 SENSOR_SERVER_LIGHT_LOG_TEXT> Log text for Light Level.
// <i> Log text for Light Level
#define SENSOR_SERVER_LIGHT_LOG_TEXT "Illuminance: %lu\r\n"

// </e>

// </h>

// <<< end of configuration section >>>

#endif // SL_BTMESH_SENSOR_SERVER_CONFIG_H
