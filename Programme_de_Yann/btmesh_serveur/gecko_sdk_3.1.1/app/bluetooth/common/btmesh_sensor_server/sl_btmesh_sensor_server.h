/***************************************************************************//**
 * @brief sl_btmesh_sensor_server.h
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BTMESH_SENSOR_SERVER_H
#define SL_BTMESH_SENSOR_SERVER_H

#include "sl_btmesh_device_properties.h"

#define SL_BTMESH_SENSOR_TEMPERATURE_VALUE_IS_NOT_KNOWN     (0xFF)
#define SL_BTMESH_SENSOR_HUMIDITY_VALUE_IS_NOT_KNOWN        (0xFF)
#define SL_BTMESH_SENSOR_ACCELARATION_VALUE_IS_NOT_KNOWN    (0xFFFFFFFF)

/**************************************************************************//**
 * Initialize Sensor Server.
 *
 * This function is called automatically by Universal Configurator after
 * enabling the component.
 *
 *****************************************************************************/
void sl_btmesh_sensor_server_node_init(void);

/**************************************************************************//**
 * Handle Sensor Server events.
 *
 * This function is called automatically by Universal Configurator after
 * enabling the component.
 *
 *****************************************************************************/
void sl_btmesh_handle_sensor_server_events(sl_btmesh_msg_t* pEvt);

/***************************************************************************//**
 * Called when a temperature measurement is done
 * @note If no implementation is provided in the application then a default weak
 *       implementation if provided which is a no-operation. (empty function)
 *
 * @param[in] temperature Temperature value in 0.5 degree Celsius steps
 ******************************************************************************/
void sl_btmesh_sensor_server_on_temperature_measurement(temperature_8_t temperature);

void sl_btmesh_sensor_server_on_humidity_measurement(percentage_8_t humidity);

#endif // SL_BTMESH_SENSOR_SERVER_H
