/***************************************************************************//**
 * @file sl_btmesh_sensor_server.c
 * @brief BT Mesh Sensor Server Instances
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc.  Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement.  This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

/* C Standard Library headers */
#include <stdio.h>
#include "sl_status.h"
/* Bluetooth stack headers */
#include "sl_bt_api.h"
#include "sl_btmesh_api.h"
#include "sl_btmesh_sensor.h"
#include "sl_btmesh_dcd.h"


#include "sl_app_log.h"
#include "sl_app_assert.h"
#include "em_common.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT

#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
#include "sl_sensor_rht_config.h"
#include "sl_sensor_rht.h"
#endif // SL_CATALOG_SENSOR_RHT_PRESENT

#include "imu_i2c.h"

#include "sl_btmesh_sensor_server.h"
#include "sl_btmesh_sensor_server_config.h"

/***************************************************************************//**
 * @addtogroup Sensor
 * @{
 ******************************************************************************/

#if SENSOR_SERVER_LOGGING == 1
#define log(...) sl_app_log(__VA_ARGS__)
#else
#define log(...)
#endif

/// The unused 0 address is used for publishing
#define PUBLISH_TO_ALL_NODES    0
/// Parameter ignored for publishing
#define IGNORED                 0
/// No flags used for message
#define NO_FLAGS                0
/// Pre-scale value for temperature sensor raw data
#define TEMPERATURE_PRE_SCALE   2
/// Offset value for temperature sensor pre-scaled value
#define TEMPERATURE_OFFSET      499
/// Scale value for temperature sensor final value
#define TEMPERATURE_SCALE_VAL   1000
/// Integer part of temperature
#define INT_TEMP(x)   (x / 2)
/// Fractional part of temperature
#define FRAC_TEMP(x)  ((x * 5) % 10)

/// Length of sensor data buffer
#define SENSOR_DATA_BUF_LEN     16
/// Property ID indicating reading every sensor
#define PROPERTY_ID_ALL         0

#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
temperature_8_t get_temperature(void);
percentage_8_t get_humidity(void);
#endif // SL_CATALOG_SENSOR_RHT_PRESENT
void get_accelaration(float* ax, float* ay, float* az);

void sl_btmesh_sensor_server_node_init(void)
{
  /// Descriptors of supported sensors
  /* The following properties are defined
   * 1. People count property (property ID: 0x004C)
   * 2. Present ambient light property (property ID: 0x004E)
   * 3. Present ambient temperature property (property ID: 0x004F)
   * NOTE: the properties must be ordered in ascending order by property ID
   */
  static const sensor_descriptor_t descriptors[] = {

#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
    {
      .property_id = PRESENT_AMBIENT_TEMPERATURE,
      .positive_tolerance = SENSOR_THERMOMETER_POSITIVE_TOLERANCE,
      .negative_tolerance = SENSOR_THERMOMETER_NEGATIVE_TOLERANCE,
      .sampling_function = SENSOR_THERMOMETER_SAMPLING_FUNCTION,
      .measurement_period = SENSOR_THERMOMETER_MEASUREMENT_PERIOD,
      .update_interval = SENSOR_THERMOMETER_UPDATE_INTERVAL
    },
    {
      .property_id = PRESENT_AMBIENT_HUMIDITY,
      .positive_tolerance = SENSOR_THERMOMETER_POSITIVE_TOLERANCE,
      .negative_tolerance = SENSOR_THERMOMETER_NEGATIVE_TOLERANCE,
      .sampling_function = SENSOR_THERMOMETER_SAMPLING_FUNCTION,
      .measurement_period = SENSOR_THERMOMETER_MEASUREMENT_PERIOD,
      .update_interval = SENSOR_THERMOMETER_UPDATE_INTERVAL
    },
    {
      .property_id = ACCELERATION_MEASURE,
      .positive_tolerance = SENSOR_THERMOMETER_POSITIVE_TOLERANCE,
      .negative_tolerance = SENSOR_THERMOMETER_NEGATIVE_TOLERANCE,
      .sampling_function = SENSOR_THERMOMETER_SAMPLING_FUNCTION,
      .measurement_period = SENSOR_THERMOMETER_MEASUREMENT_PERIOD,
      .update_interval = SENSOR_THERMOMETER_UPDATE_INTERVAL
    }
#endif // SL_CATALOG_SENSOR_RHT_PRESENT
  };

  uint16_t status = mesh_lib_sensor_server_init(BTMESH_SENSOR_SERVER_MAIN,
                                                sizeof(descriptors)
                                                / sizeof(sensor_descriptor_t),
                                                descriptors);
  sl_app_assert(status == SL_STATUS_OK,
                "[E: 0x%04x] Sensor Init Error\n",
                (int)status);


#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
  sl_sensor_rht_init();
#endif // SL_CATALOG_SENSOR_RHT_PRESENT
}

/***************************************************************************//**
 * Handling of sensor server get request event.
 * It sending sensor status message with data for all of supported Properties ID,
 * if there is no Property ID field in request. If request contains Property ID
 * that is supported, functions reply with the sensor status message with data
 * for this Property ID, in other case the message contains no data.
 *
 * @param[in] evt  Pointer to sensor server get request event.
 ******************************************************************************/
static void handle_sensor_server_get_request(
  sl_btmesh_evt_sensor_server_get_request_t *evt)
{
  // A slot for all sensor data
  uint8_t sensor_data[SENSOR_DATA_BUF_LEN];
  uint8_t len = 0;
  (void)evt;

#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
  if ((evt->property_id == PRESENT_AMBIENT_TEMPERATURE)
      || (evt->property_id == PROPERTY_ID_ALL)) {
    temperature_8_t temperature = get_temperature();
    len += mesh_sensor_data_to_buf(PRESENT_AMBIENT_TEMPERATURE,
                                   &sensor_data[len],
                                   (uint8_t*)&temperature);
  }
  if ((evt->property_id == PRESENT_AMBIENT_HUMIDITY)
      || (evt->property_id == PROPERTY_ID_ALL)) {
    percentage_8_t humidity = get_humidity();
    len += mesh_sensor_data_to_buf(PRESENT_AMBIENT_HUMIDITY,
                                   &sensor_data[len],
                                   (uint8_t*)&humidity);
  }
  if ((evt->property_id == ACCELERATION_MEASURE)
      || (evt->property_id == PROPERTY_ID_ALL)) {
    float ax,ay,az;
    get_accelaration(&ax, &ay, &az);
    len += mesh_sensor_data_to_buf(ACCELERATION_MEASURE,
                                   &sensor_data[len],
                                   (uint8_t*)&ax);
  }
#endif // SL_CATALOG_SENSOR_RHT_PRESENT
  if (len > 0) {
    sl_btmesh_sensor_server_send_status(evt->client_address,
                                        BTMESH_SENSOR_SERVER_MAIN,
                                        evt->appkey_index,
                                        NO_FLAGS,
                                        len,
                                        sensor_data);
  } else {
    sensor_data[0] = evt->property_id & 0xFF;
    sensor_data[1] = ((evt->property_id) >> 8) & 0xFF;
    sensor_data[2] = 0; // Length is 0 for unsupported property_id
    sl_btmesh_sensor_server_send_status(evt->client_address,
                                        BTMESH_SENSOR_SERVER_MAIN,
                                        evt->appkey_index,
                                        NO_FLAGS,
                                        3,
                                        sensor_data);
  }
}

/***************************************************************************//**
 * Handling of sensor server get column request event.
 * Used Property IDs does not have sensor series column state,
 * so reply has the same data as request according to specification.
 *
 * @param[in] evt  Pointer to sensor server get column request event.
 ******************************************************************************/
static void handle_sensor_server_get_column_request(
  sl_btmesh_evt_sensor_server_get_column_request_t *evt)
{
  sl_btmesh_sensor_server_send_column_status(evt->client_address,
                                             BTMESH_SENSOR_SERVER_MAIN,
                                             evt->appkey_index,
                                             NO_FLAGS,
                                             evt->property_id,
                                             evt->column_ids.len,
                                             evt->column_ids.data);
}

/***************************************************************************//**
 * Handling of sensor server get series request event.
 * Used Property IDs does not have sensor series column state,
 * so reply has only Property ID according to specification.
 *
 * @param[in] evt  Pointer to sensor server get series request event.
 ******************************************************************************/
static void handle_sensor_server_get_series_request(
  sl_btmesh_evt_sensor_server_get_series_request_t *evt)
{
  sl_btmesh_sensor_server_send_series_status(evt->client_address,
                                             BTMESH_SENSOR_SERVER_MAIN,
                                             evt->appkey_index,
                                             NO_FLAGS,
                                             evt->property_id,
                                             0,
                                             NULL);
}

/***************************************************************************//**
 * Handling of sensor server publish event.
 * It is used to signal the elapse of the publish period, when the server app
 * shall publish the sensor states
 *
 * @param[in] evt  Pointer to sensor server publish request event
 ******************************************************************************/
static void handle_sensor_server_publish_event(
  sl_btmesh_evt_sensor_server_publish_t *evt)
{
  (void)evt;
  uint8_t sensor_data[SENSOR_DATA_BUF_LEN];
  uint8_t len = 0;


#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
  temperature_8_t temperature = get_temperature();
  len += mesh_sensor_data_to_buf(PRESENT_AMBIENT_TEMPERATURE,
                                 &sensor_data[len],
                                 (uint8_t*)&temperature);

  percentage_8_t humidity = get_humidity();
  len += mesh_sensor_data_to_buf(PRESENT_AMBIENT_HUMIDITY,
                                 &sensor_data[len],
                                 (uint8_t*)&humidity);
  float ax,ay,az;
  get_accelaration(&ax, &ay, &az);
  len += mesh_sensor_data_to_buf(ACCELERATION_MEASURE,
                                 &sensor_data[len],
                                 (uint8_t*)&ax);
  sl_app_log("donne ax envoyer = %f", ax);


#endif // SL_CATALOG_SENSOR_RHT_PRESENT

  if (len > 0) {
    sl_btmesh_sensor_server_send_status(PUBLISH_TO_ALL_NODES,
                                        BTMESH_SENSOR_SERVER_MAIN,
                                        IGNORED,
                                        NO_FLAGS,
                                        len,
                                        sensor_data);
  }
}

/***************************************************************************//**
 * Handling of sensor setup server get cadence request event.
 * Cadence is not supported now, so reply has only Property ID
 * according to specification.
 *
 * @param[in] evt  Pointer to sensor server get cadence request event.
 ******************************************************************************/
static void handle_sensor_setup_server_get_cadence_request(
  sl_btmesh_evt_sensor_setup_server_get_cadence_request_t *evt)
{
  sl_btmesh_sensor_setup_server_send_cadence_status(evt->client_address,
                                                    BTMESH_SENSOR_SERVER_MAIN,
                                                    evt->appkey_index,
                                                    NO_FLAGS,
                                                    evt->property_id,
                                                    0,
                                                    NULL);
}

/***************************************************************************//**
 * Handling of sensor setup server set cadence request event.
 * Cadence is not supported now, so reply has only Property ID
 * according to specification.
 *
 * @param[in] evt  Pointer to sensor server set cadence request event.
 ******************************************************************************/
static void handle_sensor_setup_server_set_cadence_request(
  sl_btmesh_evt_sensor_setup_server_set_cadence_request_t *evt)
{
  sl_btmesh_sensor_setup_server_send_cadence_status(evt->client_address,
                                                    BTMESH_SENSOR_SERVER_MAIN,
                                                    evt->appkey_index,
                                                    NO_FLAGS,
                                                    evt->property_id,
                                                    0,
                                                    NULL);
}

/***************************************************************************//**
 * Handling of sensor setup server get settings request event.
 * Settings are not supported now, so reply has only Property ID
 * according to specification.
 *
 * @param[in] evt  Pointer to sensor server get settings request event.
 ******************************************************************************/
static void handle_sensor_setup_server_get_settings_request(
  sl_btmesh_evt_sensor_setup_server_get_settings_request_t *evt)
{
  sl_btmesh_sensor_setup_server_send_settings_status(evt->client_address,
                                                     BTMESH_SENSOR_SERVER_MAIN,
                                                     evt->appkey_index,
                                                     NO_FLAGS,
                                                     evt->property_id,
                                                     0,
                                                     NULL);
}

/***************************************************************************//**
 * Handling of sensor setup server get setting request event.
 * Settings are not supported now, so reply has only Property ID
 * and Sensor Property ID according to specification.
 *
 * @param[in] evt  Pointer to sensor server get setting request event.
 ******************************************************************************/
static void handle_sensor_setup_server_get_setting_request(
  sl_btmesh_evt_sensor_setup_server_get_setting_request_t *evt)
{
  sl_btmesh_sensor_setup_server_send_setting_status(evt->client_address,
                                                    BTMESH_SENSOR_SERVER_MAIN,
                                                    evt->appkey_index,
                                                    NO_FLAGS,
                                                    evt->property_id,
                                                    evt->setting_id,
                                                    0,
                                                    NULL);
}

/***************************************************************************//**
 * Handling of sensor setup server set setting request event.
 * Settings are not supported now, so reply has only Property ID
 * and Sensor Property ID according to specification.
 *
 * @param[in] evt  Pointer to sensor server set setting request event.
 ******************************************************************************/
static void handle_sensor_setup_server_set_setting_request(
  sl_btmesh_evt_sensor_setup_server_set_setting_request_t *evt)
{
  sl_btmesh_sensor_setup_server_send_setting_status(evt->client_address,
                                                    BTMESH_SENSOR_SERVER_MAIN,
                                                    evt->appkey_index,
                                                    NO_FLAGS,
                                                    evt->property_id,
                                                    evt->setting_id,
                                                    0,
                                                    NULL);
}

/***************************************************************************//**
 *  Handling of mesh events by sensor server component.
 *  It handles:
 *   - node_initialized
 *   - node_provisioned
 *   - sensor_server_get_request
 *   - sensor_server_get_column_request
 *   - sensor_server_get_series_request
 *   - sensor_setup_server_get_cadence_request
 *   - sensor_setup_server_set_cadence_request
 *   - sensor_setup_server_get_settings_request
 *   - sensor_setup_server_get_setting_request
 *   - sensor_setup_server_set_setting_request
 *
 *  @param[in] evt  Pointer to incoming sensor server event.
 ******************************************************************************/
void sl_btmesh_handle_sensor_server_events(sl_btmesh_msg_t* evt)
{
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_btmesh_evt_node_initialized_id:
      if (evt->data.evt_node_initialized.provisioned) {
        sl_btmesh_sensor_server_node_init();
      }
      break;

    case sl_btmesh_evt_node_provisioned_id:
      sl_btmesh_sensor_server_node_init();
      break;

    case sl_btmesh_evt_sensor_server_get_request_id:
      handle_sensor_server_get_request(
        &(evt->data.evt_sensor_server_get_request));
      break;

    case sl_btmesh_evt_sensor_server_get_column_request_id:
      handle_sensor_server_get_column_request(
        &(evt->data.evt_sensor_server_get_column_request));
      break;

    case sl_btmesh_evt_sensor_server_get_series_request_id:
      handle_sensor_server_get_series_request(
        &(evt->data.evt_sensor_server_get_series_request));
      break;

    case sl_btmesh_evt_sensor_server_publish_id:
      handle_sensor_server_publish_event(
        &(evt->data.evt_sensor_server_publish));
      break;

    case sl_btmesh_evt_sensor_setup_server_get_cadence_request_id:
      handle_sensor_setup_server_get_cadence_request(
        &(evt->data.evt_sensor_setup_server_get_cadence_request));
      break;

    case sl_btmesh_evt_sensor_setup_server_set_cadence_request_id:
      handle_sensor_setup_server_set_cadence_request(
        &(evt->data.evt_sensor_setup_server_set_cadence_request));
      break;

    case sl_btmesh_evt_sensor_setup_server_get_settings_request_id:
      handle_sensor_setup_server_get_settings_request(
        &(evt->data.evt_sensor_setup_server_get_settings_request));
      break;

    case sl_btmesh_evt_sensor_setup_server_get_setting_request_id:
      handle_sensor_setup_server_get_setting_request(
        &(evt->data.evt_sensor_setup_server_get_setting_request));
      break;

    case sl_btmesh_evt_sensor_setup_server_set_setting_request_id:
      handle_sensor_setup_server_set_setting_request(
        &(evt->data.evt_sensor_setup_server_set_setting_request));
      break;

    default:
      break;
  }
}

#ifdef SL_CATALOG_SENSOR_RHT_PRESENT
/***************************************************************************//**
 * Get the current temperature value measured by sensor.
 *
 * @return Current value of temperature.
 ******************************************************************************/
temperature_8_t get_temperature(void)
{
  int32_t temp_data = 0;
  uint32_t temp_rh = 0;
  temperature_8_t temperature;
  sl_status_t sc = sl_sensor_rht_get(&temp_rh, &temp_data);
  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid temperature reading: %lu %ld\n",
               temp_rh,
               temp_data);
    temperature = SL_BTMESH_SENSOR_TEMPERATURE_VALUE_IS_NOT_KNOWN;
  } else {
    temp_data = (((temp_data
                   * TEMPERATURE_PRE_SCALE)
                  + TEMPERATURE_OFFSET)
                 / TEMPERATURE_SCALE_VAL);
    temperature = (temperature_8_t)temp_data;
  }
  sl_btmesh_sensor_server_on_temperature_measurement(temperature);
  return temperature;
}

/***************************************************************************//**
 * Get the current humidity value measured by sensor.
 *
 * @return Current value of humidity.
 ******************************************************************************/
percentage_8_t get_humidity(void)
{
  int32_t temp_data = 0;
  uint32_t temp_rh = 0;
  percentage_8_t humidity;
  sl_status_t sc = sl_sensor_rht_get(&temp_rh, &temp_data);
  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid humidity reading: %lu %ld\n",
               temp_rh,
               temp_data);
    humidity = SL_BTMESH_SENSOR_HUMIDITY_VALUE_IS_NOT_KNOWN;
  } else {
      temp_rh = (((temp_rh
                   * TEMPERATURE_PRE_SCALE)
                  + TEMPERATURE_OFFSET)
                 / TEMPERATURE_SCALE_VAL);
    humidity = (percentage_8_t)temp_rh;
  }
  sl_btmesh_sensor_server_on_humidity_measurement(humidity);
  return humidity;
}
#endif // SL_CATALOG_SENSOR_RHT_PRESENT

void get_accelaration(float* ax, float* ay, float* az)
{
  int16_t mpu[3];
  float aRes;
  aRes = 2.0f / 32768.0f;

  sl_status_t sc = mpu_get(mpu);

  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid IMU reading");

    *ax = (float)SL_BTMESH_SENSOR_ACCELARATION_VALUE_IS_NOT_KNOWN;
    *ay = (float)SL_BTMESH_SENSOR_ACCELARATION_VALUE_IS_NOT_KNOWN;
    *az = (float)SL_BTMESH_SENSOR_ACCELARATION_VALUE_IS_NOT_KNOWN;

  } else {
      *ax = (float)mpu[0] * aRes * 1000; // - myIMU.accelBias[0];
      *ay = (float)mpu[1] * aRes * 1000; // - myIMU.accelBias[1];
      *az = (float)mpu[2] * aRes * 1000; // - myIMU.accelBias[2];


      sl_app_log("ax : %f ; ay : %f ; az : %f \n",*ax,*ay,*az);
  }
}


/**************************************************************************//**
 * @addtogroup btmesh_sens_srv_cb_weak Weak implementation of callbacks
 * @{
 *****************************************************************************/
SL_WEAK void sl_btmesh_sensor_server_on_temperature_measurement(temperature_8_t temperature)
{
  if ((temperature_8_t)SL_BTMESH_SENSOR_TEMPERATURE_VALUE_IS_NOT_KNOWN
      == temperature) {
    sl_app_log("Temperature: UNKNOWN\r\n");
  } else {
    sl_app_log("Temperature: %3d.%1dC\r\n",
               INT_TEMP(temperature),
               FRAC_TEMP(temperature));
  }
}

SL_WEAK void sl_btmesh_sensor_server_on_humidity_measurement(percentage_8_t humidity)
{
  if ((percentage_8_t)SL_BTMESH_SENSOR_HUMIDITY_VALUE_IS_NOT_KNOWN
      == humidity) {
    sl_app_log("Humidity: UNKNOWN\r\n");
  } else {
    sl_app_log("Humidity: %3d.%1d%%\r\n",
               INT_TEMP(humidity),
               FRAC_TEMP(humidity));
  }
}
/** @} (end addtogroup btmesh_sens_srv_cb_weak) */

/** @} (end addtogroup Sensor) */
