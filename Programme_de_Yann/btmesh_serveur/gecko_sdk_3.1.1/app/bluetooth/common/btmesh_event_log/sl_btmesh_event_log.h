/***************************************************************************//**
 * @brief btmesh_event_log.h
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BTMESH_EVENT_LOG
#define SL_BTMESH_EVENT_LOG

/***************************************************************************//**
 * Handling of sensor client stack events. Both BLuetooth LE and Bluetooth mesh
 * events are handled here.
 * @param[in] evt_id  Incoming event ID.
 * @param[in] evt     Pointer to incoming event.
 ******************************************************************************/
void sl_btmesh_handle_btmesh_logging_events(sl_btmesh_msg_t *evt);

#endif // SL_BTMESH_EVENT_LOG
