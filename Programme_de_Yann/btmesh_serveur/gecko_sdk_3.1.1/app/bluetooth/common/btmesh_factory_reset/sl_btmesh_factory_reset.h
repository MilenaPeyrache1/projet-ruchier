/***************************************************************************//**
 * @brief factory_reset.h
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BTMESH_FACTORY_RESET_H
#define SL_BTMESH_FACTORY_RESET_H

/***************************************************************************//**
 * This function is called to initiate factory reset.
 ******************************************************************************/
void sl_btmesh_initiate_factory_reset(void);

/***************************************************************************//**
 * Called when factory reset is established, before system halt
 * @note If no implementation is provided in the application then a default weak
 *       implementation if provided which is a no-operation. (empty function)
 *
 ******************************************************************************/
void sl_btmesh_factory_reset_on_reset(void);

#endif // SL_BTMESH_FACTORY_RESET_H
