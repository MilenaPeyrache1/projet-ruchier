/***************************************************************************//**
 * @file  sl_btmesh_provisioning_decorator.c
 * @brief Provisioning decorator module
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include "sl_status.h"
#include "sl_bt_api.h"
#include "sl_btmesh_api.h"

#include "em_common.h"
#include "sl_app_log.h"
#include "sl_app_assert.h"
#include "sl_simple_timer.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT

#include "sl_btmesh_provisioning_decorator.h"
#include "sl_btmesh_provisioning_decorator_config.h"

/***************************************************************************//**
 * @addtogroup ProvisioningDecorator
 * @{
 ******************************************************************************/

#if PROVISIONING_DECORATOR_LOGGING == 1
#define log(...) sl_app_log(__VA_ARGS__)
#else
#define log(...)
#endif

/// High Priority
#define HIGH_PRIORITY                  0
/// No Timer Options
#define NO_FLAGS                       0
/// Callback has no parameters
#define NO_CALLBACK_DATA               (void *)NULL

// periodic timer handle
static sl_simple_timer_t restart_timer;
// periodic timer callback
static void prov_decor_restart_timer_cb(sl_simple_timer_t *handle,
                                        void *data);

// -----------------------------------------------------------------------------
// Provisioning Callbacks

/*******************************************************************************
 * Called at node initialization time to provide provisioning information
 *
 * @param[in] provisioned  true: provisioned, false: unprovisioned
 * @param[in] address      Unicast address of the primary element of the node.
                           Ignored if unprovisioned.
 * @param[in] iv_index     IV index for the first network of the node
                           Ignored if unprovisioned.
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 ******************************************************************************/
SL_WEAK void sl_btmesh_on_provision_init_status(bool provisioned,
                                                uint16_t address,
                                                uint32_t iv_index)
{
  (void) provisioned;
  (void) address;
  (void) iv_index;
}

/*******************************************************************************
 * Called when the Provisioning starts
 *
 * @param[in] result  Result code. 0: success, non-zero: error
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 ******************************************************************************/
SL_WEAK void sl_btmesh_on_node_provisioning_started(uint16_t result)
{
  (void) result;
}

/*******************************************************************************
 * Called when the Provisioning finishes successfully
 *
 * @param[in] address      Unicast address of the primary element of the node.
                           Ignored if unprovisioned.
 * @param[in] iv_index     IV index for the first network of the node
                           Ignored if unprovisioned.

 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 ******************************************************************************/
SL_WEAK void sl_btmesh_on_node_provisioned(uint16_t address,
                                           uint32_t iv_index)
{
  (void) address;
  (void) iv_index;
}

/*******************************************************************************
 * Called when the Provisioning fails
 *
 * @param[in] result  Result code. 0: success, non-zero: error
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 ******************************************************************************/
SL_WEAK void sl_btmesh_on_node_provisioning_failed(uint16_t result)
{
  (void) result;
}

// -----------------------------------------------------------------------------
// Provisioning Decorator Callbacks

/*******************************************************************************
 * Handling of Provisioning Decorator stack events.
 *
 * @param[in] evt  Event type
 ******************************************************************************/
void sl_btmesh_handle_provisioning_decorator_event(sl_btmesh_msg_t *evt)
{
  if (NULL == evt) {
    return;
  }

  // Handle events
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_btmesh_evt_node_initialized_id:
      sl_btmesh_on_provision_init_status(evt->data.evt_node_initialized.provisioned,
                                         evt->data.evt_node_initialized.address,
                                         evt->data.evt_node_initialized.iv_index);
      break;

    case sl_btmesh_evt_node_provisioning_started_id:
      sl_btmesh_on_node_provisioning_started(evt->data.evt_node_provisioning_started.result);
      break;

    case sl_btmesh_evt_node_provisioned_id:
      sl_btmesh_on_node_provisioned(evt->data.evt_node_provisioned.address,
                                    evt->data.evt_node_provisioned.iv_index);
      break;

    case sl_btmesh_evt_node_provisioning_failed_id: {
      sl_btmesh_on_node_provisioning_failed(evt->data.evt_node_provisioning_failed.result);

      log("Btmesh system reset timer is started with %dms timeout.\r\n",
          PROVISIONING_DECORATOR_RESTART_TIMER_TIMEOUT);

      sl_status_t sc = sl_simple_timer_start(&restart_timer,
                                             PROVISIONING_DECORATOR_RESTART_TIMER_TIMEOUT,
                                             prov_decor_restart_timer_cb,
                                             NO_CALLBACK_DATA,
                                             false);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start timer\n",
                    (int)sc);
      break;
    }

    default:
      break;
  }
}

/***************************************************************************//**
 * Called when the restart timer expires.
 *
 * @param[in] handle  Pointer to the timer handle
 * @param[in] data    Pointer to callback data
 ******************************************************************************/
static void prov_decor_restart_timer_cb(sl_simple_timer_t *handle,
                                        void *data)
{
  (void)data;
  (void)handle;
  sl_bt_system_reset(0);
}

/** @} (end addtogroup ProvisioningDecorator) */
