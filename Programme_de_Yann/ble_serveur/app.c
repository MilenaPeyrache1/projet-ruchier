/*
 * app.c
 *
 *  Created on: 9 janv. 2021
 *      Author: yann le gall
 *
 *  Lecture d'un capteur I2C et envoie de la donnée par bluetooth
 *  Donnée envoyé dans une caractéristique de type Weight.
 *
 */

#include <stdbool.h>
#include "em_common.h"
#include "sl_status.h"
#include "sl_simple_button_instances.h"
#include "sl_simple_led_instances.h"
#include "sl_simple_timer.h"
#include "sl_app_log.h"
#include "sl_app_assert.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT
#ifdef SL_CATALOG_CLI_PRESENT
#include "sl_cli.h"
#endif // SL_CATALOG_CLI_PRESENT
#include "app.h"
#include "imu_i2c.h"
#include "sl_sensor_rht.h"

// Connection handle.
static uint8_t app_connection = 0;

// The advertising set handle allocated from Bluetooth stack.
static uint8_t advertising_set_handle = 0xff;

// Button state.
static volatile bool app_btn0_pressed = false;

// Periodic timer handle.
static sl_simple_timer_t weight_app_periodic_timer;
static sl_simple_timer_t temperature_app_periodic_timer;
static sl_simple_timer_t humidity_app_periodic_timer;

// Periodic timer callback.
static void weight_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data);
static void temperature_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data);
static void humidity_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data);

// Declaration de fonctions
// weight
sl_status_t weight_measurement_indicate(uint8_t connection, float value);
void weight_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config);
static void weight_measurement_val_to_buf(float value, uint8_t *buffer);
void weight_measurement_indication_confirmed_cb(uint8_t connection);

// temperature

sl_status_t temperature_measurement_indicate(uint8_t connection, int32_t value);
void temperature_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config);
static void temperature_measurement_val_to_buf(int32_t value, uint8_t *buffer);
void temperature_measurement_indication_confirmed_cb(uint8_t connection);

// humidity
sl_status_t humidity_measurement_indicate(uint8_t connection, uint32_t value);
void humidity_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config);
static void humidity_measurement_val_to_buf(uint32_t value, uint8_t *buffer);
void humidity_measurement_indication_confirmed_cb(uint8_t connection);

/**************************************************************************//**
 * Application Init.
 *****************************************************************************/
SL_WEAK void app_init(void)
{
  sl_app_log("mesure capteur I2C initialised\n");
  sl_sensor_rht_init();
}

#ifndef SL_CATALOG_KERNEL_PRESENT
/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
}
#endif

/**************************************************************************//**
 * Bluetooth stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void sl_bt_on_event(sl_bt_msg_t *evt)
{
  sl_status_t sc;
  bd_addr address;
  uint8_t address_type;
  uint8_t system_id[8];

  // Handle stack events
  switch (SL_BT_MSG_ID(evt->header)) {
    // -------------------------------
    // This event indicates the device has started and the radio is ready.
    // Do not call any stack command before receiving this boot event!
    case sl_bt_evt_system_boot_id:

      // Print boot message.
      sl_app_log("Bluetooth stack booted: v%d.%d.%d-b%d\n",
                 evt->data.evt_system_boot.major,
                 evt->data.evt_system_boot.minor,
                 evt->data.evt_system_boot.patch,
                 evt->data.evt_system_boot.build);

      // Extract unique ID from BT Address.
      sc = sl_bt_system_get_identity_address(&address, &address_type);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to get Bluetooth address\n",
                    (int)sc);

      // Pad and reverse unique ID to get System ID.
      system_id[0] = address.addr[5];
      system_id[1] = address.addr[4];
      system_id[2] = address.addr[3];
      system_id[3] = 0xFF;
      system_id[4] = 0xFE;
      system_id[5] = address.addr[2];
      system_id[6] = address.addr[1];
      system_id[7] = address.addr[0];

      sc = sl_bt_gatt_server_write_attribute_value(gattdb_system_id,
                                                   0,
                                                   sizeof(system_id),
                                                   system_id);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to write attribute\n",
                    (int)sc);

      sl_app_log("Bluetooth %s address: %02X:%02X:%02X:%02X:%02X:%02X\n",
                 address_type ? "static random" : "public device",
                 address.addr[5],
                 address.addr[4],
                 address.addr[3],
                 address.addr[2],
                 address.addr[1],
                 address.addr[0]);

      // Create an advertising set.
      sc = sl_bt_advertiser_create_set(&advertising_set_handle);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to create advertising set\n",
                    (int)sc);

      // Set advertising interval to 100ms.
      sc = sl_bt_advertiser_set_timing(
        advertising_set_handle, // advertising set handle
        160, // min. adv. interval (milliseconds * 1.6)
        160, // max. adv. interval (milliseconds * 1.6)
        0,   // adv. duration
        0);  // max. num. adv. events
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to set advertising timing\n",
                    (int)sc);
      // Start general advertising and enable connections.
      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      sl_app_log("Started advertising\n");
      break;

    // -------------------------------
    // This event indicates that a new connection was opened.
    case sl_bt_evt_connection_opened_id:
      sl_app_log("Connection opened\n");
      break;

    // -------------------------------
    // This event indicates that a connection was closed.
    case sl_bt_evt_connection_closed_id:
    sl_bt_connection_closed_cb(evt->data.evt_connection_closed.reason,
               evt->data.evt_connection_closed.connection);
      sl_app_log("Connection closed\n");
      // Restart advertising after client has disconnected.
      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      sl_app_log("Started advertising\n");
      break;

    case sl_bt_evt_gatt_server_characteristic_status_id:
      // gestion de l'actualisation du poids
      if (gattdb_weight_measurement == evt->data.evt_gatt_server_characteristic_status.characteristic) {
        // client characteristic configuration changed by remote GATT client
        if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            weight_measurement_indication_changed_cb(
            evt->data.evt_gatt_server_characteristic_status.connection,
            evt->data.evt_gatt_server_characteristic_status.client_config_flags);
        }
        // confirmation of indication received from remove GATT client
        else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
          weight_measurement_indication_confirmed_cb(evt->data.evt_gatt_server_characteristic_status.connection);
        } else {
          sl_app_assert(false,
                        "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                        (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
        }
      }

      // gestion de l'actualisation de la temperature
      if (gattdb_temperature == evt->data.evt_gatt_server_characteristic_status.characteristic) {
        // client characteristic configuration changed by remote GATT client
        if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            temperature_measurement_indication_changed_cb(
            evt->data.evt_gatt_server_characteristic_status.connection,
            evt->data.evt_gatt_server_characteristic_status.client_config_flags);
        }
        // confirmation of indication received from remove GATT client
        else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            temperature_measurement_indication_confirmed_cb(evt->data.evt_gatt_server_characteristic_status.connection);
        } else {
          sl_app_assert(false,
                        "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                        (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
        }
      }

      // gestion de l'actualisation de l'humidite
      if (gattdb_humidity == evt->data.evt_gatt_server_characteristic_status.characteristic) {
        // client characteristic configuration changed by remote GATT client
        if (gatt_server_client_config == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            humidity_measurement_indication_changed_cb(
            evt->data.evt_gatt_server_characteristic_status.connection,
            evt->data.evt_gatt_server_characteristic_status.client_config_flags);
        }
        // confirmation of indication received from remove GATT client
        else if (gatt_server_confirmation == (gatt_server_characteristic_status_flag_t)evt->data.evt_gatt_server_characteristic_status.status_flags) {
            humidity_measurement_indication_confirmed_cb(evt->data.evt_gatt_server_characteristic_status.connection);
        } else {
          sl_app_assert(false,
                        "[E: 0x%04x] Unexpected status flag in evt_gatt_server_characteristic_status\n",
                        (int)evt->data.evt_gatt_server_characteristic_status.status_flags);
        }
      }
      break;

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}

/**************************************************************************//**
 * Callback function of connection close event.
 *
 * @param[in] reason Unused parameter required by the health_thermometer component
 * @param[in] connection Unused parameter required by the health_thermometer component
 *****************************************************************************/
void sl_bt_connection_closed_cb(uint16_t reason, uint8_t connection)
{
  (void)reason;
  (void)connection;
  sl_status_t sc;

  // Stop timer.
  sc = sl_simple_timer_stop(&weight_app_periodic_timer);
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to stop periodic timer\n",
                (int)sc);
}

/**************************************************************************//**
 * Simple Button
 * Button state changed callback
 * @param[in] handle Button event handle
 *****************************************************************************/
void sl_button_on_change(const sl_button_t *handle)
{
  // Button pressed.
  if (sl_button_get_state(handle) == SL_SIMPLE_BUTTON_PRESSED) {
    if (&sl_button_btn0 == handle) {
      sl_led_turn_on(&sl_led_led0);
      app_btn0_pressed = true;
    }
  }
  // Button released.
  else if (sl_button_get_state(handle) == SL_SIMPLE_BUTTON_RELEASED) {
    if (&sl_button_btn0 == handle) {
      sl_led_turn_off(&sl_led_led0);
      app_btn0_pressed = false;
    }
  }
}

#ifdef SL_CATALOG_CLI_PRESENT
void hello(sl_cli_command_arg_t *arguments)
{
  (void) arguments;
  bd_addr address;
  uint8_t address_type;
  sl_status_t sc = sl_bt_system_get_identity_address(&address, &address_type);
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to get Bluetooth address\n",
                (int)sc);
  sl_app_log("Bluetooth %s address: %02X:%02X:%02X:%02X:%02X:%02X\n",
             address_type ? "static random" : "public device",
             address.addr[5],
             address.addr[4],
             address.addr[3],
             address.addr[2],
             address.addr[1],
             address.addr[0]);
}
#endif // SL_CATALOG_CLI_PRESENT



////////////////////////////////// weight /////////////////////////////////////////

void weight_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config)
{
  sl_status_t sc;
  app_connection = connection;
  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    // Start timer used for periodic indications.
    sc = sl_simple_timer_start(&weight_app_periodic_timer,
                               WEIGHT_MEASUREMENT_INTERVAL_SEC * 1000,
                               weight_app_periodic_timer_cb,
                               NULL,
                               true);
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start periodic timer\n",
                  (int)sc);
    // Send first indication.
    weight_app_periodic_timer_cb(&weight_app_periodic_timer, NULL);
  }
  // Indications disabled.
  else {
    // Stop timer used for periodic indications.
    (void)sl_simple_timer_stop(&weight_app_periodic_timer);
  }
}

void weight_measurement_indication_confirmed_cb(uint8_t connection)
{
  (void)connection;
}

static void weight_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
  sl_status_t sc;
  int16_t mpu[3];
  float ax, ay, az;
  float aRes;

  aRes = 2.0f / 32768.0f;

  sc = mpu_get(mpu);

  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid IMU reading");
  }
  else {
    ax = (float)mpu[0] * aRes * 1000; // - myIMU.accelBias[0];
    ay = (float)mpu[1] * aRes * 1000; // - myIMU.accelBias[1];
    az = (float)mpu[2] * aRes * 1000; // - myIMU.accelBias[2];

    // button 0 pressed: overwrite temperature with -20C.
    if (app_btn0_pressed) {
      ax = 650;
    }

    sl_app_log("ax : %f ; ay : %f ; az : %f \n",ax,ay,az);

    // Send temperature measurement indication to connected client.
    sc = weight_measurement_indicate(app_connection, ax*100);

    if (sc) {
      sl_app_log("Warning! Failed to send weight measurement indication\n");
    }
  }
}


sl_status_t weight_measurement_indicate(uint8_t connection, float value)
{
  sl_status_t sc;
  uint16_t len = 0;
  uint8_t buf[4] = { 0 };
  weight_measurement_val_to_buf(value, buf);
  sc = sl_bt_gatt_server_send_characteristic_notification(
    connection,
    gattdb_weight_measurement,
    sizeof(buf),
    buf,
    &len);
  return sc;
}

static void weight_measurement_val_to_buf(float value, uint8_t *buffer)
{
  //sl_app_log("value int32 : %f\n",value);
  uint32_t tmp_value = ((uint32_t)value & 0x0000ffffu) \
                       | ((uint32_t)(-3) << 16);
  //sl_app_log("tmp_value uint32 : %ld\n",tmp_value);
  buffer[0] = 0;
  buffer[1] = tmp_value & 0xff;
  buffer[2] = (tmp_value >> 8) & 0xff;
  buffer[3] = (tmp_value >> 16) & 0xff;
  //sl_app_log("b0 : %x, b1 : %x, b2 : %x, b3 : %x\n",buffer[0],buffer[1],buffer[2],buffer[3]);
}


////////////////////////////////// temperature /////////////////////////////////////////

void temperature_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config)
{
  sl_status_t sc;
  app_connection = connection;
  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    // Start timer used for periodic indications.
    sc = sl_simple_timer_start(&temperature_app_periodic_timer,
                               TEMPERATURE_MEASUREMENT_INTERVAL_SEC * 1000,
                               temperature_app_periodic_timer_cb,
                               NULL,
                               true);
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start periodic timer\n",
                  (int)sc);
    // Send first indication.
    temperature_app_periodic_timer_cb(&temperature_app_periodic_timer, NULL);
  }
  // Indications disabled.
  else {
    // Stop timer used for periodic indications.
    (void)sl_simple_timer_stop(&temperature_app_periodic_timer);
  }
}

void temperature_measurement_indication_confirmed_cb(uint8_t connection)
{
  (void)connection;
}

static void temperature_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
  sl_status_t sc;
  int32_t temperature = 0;
  uint32_t humidity = 0;
  float tmp_c = 0.0;

  // Measure humidity and temperature; units are % and milli-Celsius.
  sc = sl_sensor_rht_get(&humidity, &temperature);
  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid RHT reading: %lu %ld\n", humidity, temperature);
  }

  if(app_btn0_pressed){
      temperature = 0;
  }

  tmp_c = (float)temperature / 1000 ;
  sl_app_log("Temperature: %5.3f C\n", tmp_c);
  // Send temperature measurement indication to connected client.
  sc = temperature_measurement_indicate(app_connection, temperature);
  if (sc) {
    sl_app_log("Warning! Failed to send temperature measurement indication\n");
  }
}

sl_status_t temperature_measurement_indicate(uint8_t connection, int32_t value)
{
 sl_status_t sc;
 uint16_t len = 0;
 uint8_t buf[4] = { 0 };
 temperature_measurement_val_to_buf(value, buf);
 sc = sl_bt_gatt_server_send_characteristic_notification(
   connection,
   gattdb_temperature,
   sizeof(buf),
   buf,
   &len);
 return sc;
}

static void temperature_measurement_val_to_buf(int32_t value, uint8_t *buffer)
{
  uint32_t tmp_value = value/10;

  buffer[0] = tmp_value & 0xff;
  buffer[1] = (tmp_value >> 8) & 0xff;
  buffer[2] = (tmp_value >> 16) & 0xff;
  buffer[3] = (tmp_value >> 24) & 0xff;
}


////////////////////////////////// humidity /////////////////////////////////////////

void humidity_measurement_indication_changed_cb(uint8_t connection, gatt_client_config_flag_t client_config)
{
  sl_status_t sc;
  app_connection = connection;
  // Indication or notification enabled.
  if (gatt_disable != client_config) {
    // Start timer used for periodic indications.
    sc = sl_simple_timer_start(&humidity_app_periodic_timer,
                               HUMIDITY_MEASUREMENT_INTERVAL_SEC * 1000,
                               humidity_app_periodic_timer_cb,
                               NULL,
                               true);
    sl_app_assert(sc == SL_STATUS_OK,
                  "[E: 0x%04x] Failed to start periodic timer\n",
                  (int)sc);
    // Send first indication.
    humidity_app_periodic_timer_cb(&humidity_app_periodic_timer, NULL);
  }
  // Indications disabled.
  else {
    // Stop timer used for periodic indications.
    (void)sl_simple_timer_stop(&humidity_app_periodic_timer);
  }
}

void humidity_measurement_indication_confirmed_cb(uint8_t connection)
{
  (void)connection;
}

static void humidity_app_periodic_timer_cb(sl_simple_timer_t *timer, void *data)
{
  (void)data;
  (void)timer;
  sl_status_t sc;
  int32_t temperature = 0;
  uint32_t humidity = 0;
  float humd = 0;

  // Measure humidity and temperature; units are % and milli-Celsius.
  sc = sl_sensor_rht_get(&humidity, &temperature);
  if (sc != SL_STATUS_OK) {
    sl_app_log("Warning! Invalid RHT reading: %lu %ld\n", humidity, temperature);
  }

  humd = (float)humidity / 1000 ;
  sl_app_log("Humidity: %5.3f Pourcent\n", humd);
  // Send humidity measurement indication to connected client.
  sc = humidity_measurement_indicate(app_connection, humidity);
  if (sc) {
    sl_app_log("Warning! Failed to send humidity measurement indication\n");
  }
}


sl_status_t humidity_measurement_indicate(uint8_t connection, uint32_t value)
{
 sl_status_t sc;
 uint16_t len = 0;
 uint8_t buf[4] = { 0 };
 humidity_measurement_val_to_buf(value, buf);
 sc = sl_bt_gatt_server_send_characteristic_notification(
   connection,
   gattdb_humidity,
   sizeof(buf),
   buf,
   &len);
 return sc;
}

static void humidity_measurement_val_to_buf(uint32_t value, uint8_t *buffer)
{
  uint32_t tmp_value = value/10;

  buffer[0] = tmp_value & 0xff;
  buffer[1] = (tmp_value >> 8) & 0xff;
  buffer[2] = (tmp_value >> 16) & 0xff;
  buffer[3] = (tmp_value >> 24) & 0xff;
}


