/***************************************************************************//**
 * @file
 * @brief Application interface provided to main().
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef APP_H
#define APP_H


#include <stddef.h>
#include <stdint.h>

#define WEIGHT_MEASUREMENT_INTERVAL_SEC   5
#define TEMPERATURE_MEASUREMENT_INTERVAL_SEC   1
#define HUMIDITY_MEASUREMENT_INTERVAL_SEC   3


/**************************************************************************//**
 * Application Init.
 *****************************************************************************/
void app_init(void);

/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
void app_process_action(void);

/**************************************************************************//**
 * Callback to handle connection closed event.
 * @param[in] reason Result code.
 * @param[in] connection Handle of the closed connection
 * @note To be implemented in user code.
 *****************************************************************************/
void sl_bt_connection_closed_cb(uint16_t reason, uint8_t connection);


#endif // APP_H


