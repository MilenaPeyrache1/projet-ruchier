/***************************************************************************//**
 * @brief sl_btmesh_sensor_client.h
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BTMESH_SENSOR_CLIENT_H
#define SL_BTMESH_SENSOR_CLIENT_H

#include "sl_btmesh_sensor.h"
#include "sl_status.h"
#include "sl_enum.h"

// -----------------------------------------------------------------------------
// Sensor Data Constants which represents the unknown value of each sensors

#define SL_BTMESH_SENSOR_CLIENT_TEMPERATURE_UNKNOWN  ((temperature_8_t) 0xFF)
#define SL_BTMESH_SENSOR_CLIENT_HUMIDITY_UNKNOWN  ((percentage_8_t) 0xFF)
#define SL_BTMESH_SENSOR_CLIENT_ACCELERATION_UNKNOWN  ((float) 0xFFFFFFFF)


/***************************************************************************//**
 * Enumeration representing the status of the sensor data received from the
 * sensor server.
 ******************************************************************************/
SL_ENUM(sl_btmesh_sensor_client_data_status_t){
  /// Valid sensor data
  SL_BTMESH_SENSOR_CLIENT_DATA_VALID = 0,

  /// No valid measured data is available
  SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN = 1,

  /// No sensor is available on the sensor server
  SL_BTMESH_SENSOR_CLIENT_DATA_NOT_AVAILABLE = 2
};

// -----------------------------------------------------------------------------
// Callbacks

/***************************************************************************//**
 * Called when Sensor Server discovery is started
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 ******************************************************************************/
void sl_btmesh_sensor_client_on_discovery_started(uint16_t property_id);

/***************************************************************************//**
 * Called when a Device with the current Device Property ID was found
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 *
 * @param[in] property_id  New property ID for which the new device
 *                         was registered.
 * @param[in] address      Address of the new device.
 ******************************************************************************/
void sl_btmesh_sensor_client_on_new_device_found(uint16_t property_id,
                                                 uint16_t address);

/***************************************************************************//**
 * Called when temperature sensor data is received from one of the
 * registered devices
 *
 * This is a callback which can be implemented in the application.
 * @note If no implementation is provided in the application then a default weak
 *       implementation is provided which is a no-operation. (empty function)
 *
 * @param[in] sensor_idx  The sensor index represents the order the sensor
 *                        servers were registered.
 * @param[in] address     Address of the sensor server.
 * @param[in] status      Determines if the data is valid, available or unknown
 * @param[in] temperature Measured temperature on the sensor server
 *                        in 0.5 degree Celsius steps.
 ******************************************************************************/
void sl_btmesh_sensor_client_on_new_temperature_data(uint8_t sensor_idx,
                                                     uint16_t address,
                                                     sl_btmesh_sensor_client_data_status_t status,
                                                     temperature_8_t temperature);

void sl_btmesh_sensor_client_on_new_humidity_data(uint8_t sensor_idx,
                                                     uint16_t address,
                                                     sl_btmesh_sensor_client_data_status_t status,
                                                     percentage_8_t humidity);

void sl_btmesh_sensor_client_on_new_acceleration_data(uint8_t sensor_idx,
                                                             uint16_t address,
                                                             sl_btmesh_sensor_client_data_status_t status,
                                                             coefficient_t ax);


// -----------------------------------------------------------------------------
// Functions

/***************************************************************************//**
 * Updating the list of registered devices.
 * All previously registered device will be lost.
 *
 * Generated callback:
 * SL_WEAK void sl_btmesh_sensor_client_on_discovery_started(uint16_t property_id);
 * SL_WEAK void sl_btmesh_sensor_client_on_new_device_found(uint16_t property_id,
 *                                                          uint16_t address);
 *
 * @param[in] property  New property ID for which devices need to be
 *                      registered.
 *
 * @return Status of the operation.
 *         Returns SL_STATUS_OK(0) if succeeded, non-zero otherwise.
 ******************************************************************************/
sl_status_t sl_btmesh_sensor_client_update_registered_devices(mesh_device_properties_t property);

/***************************************************************************//**
 * Requesting new sensor data for the currently selected sensor property ID.
 *
 * Generated callback:
 *
 * Called callback signature depends on the received sensor data type.
 * The <> symbols marks the part which is different due to the different sensor
 * types in these callbacks.
 *
 * SL_WEAK void sl_btmesh_sensor_client_on_new_<sensor>_data(
 *                          uint8_t sensor_idx,
 *                          uint16_t address,
 *                          sl_btmesh_sensor_client_data_status_t status,
 *                          <sensor_data_type_t> <sensor_data>)
 *
 * @param[in] property  New property ID for which devices need to be registered.
 *
 * @return Status of the operation.
 *         Returns SL_STATUS_OK(0) if succeeded, non-zero otherwise.
 ******************************************************************************/
sl_status_t sl_btmesh_sensor_client_get_sensor_data(mesh_device_properties_t property);

// -----------------------------------------------------------------------------
// Functions which are automatically called when the component is selected

/***************************************************************************//**
 * Handle Sensor Client events.
 *
 * This function is called automatically by Universal Configurator after
 * enabling the component.
 *
 * @param[in] evt Pointer to incoming event
 ******************************************************************************/
void sl_btmesh_handle_sensor_client_on_event(sl_btmesh_msg_t *evt);

#endif // SL_BTMESH_SENSOR_CLIENT_H
