/***************************************************************************//**
 * @file  sl_btmesh_sensor_client.c
 * @brief Sensor client module
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include "em_common.h"
#include "sl_status.h"
#include "sl_bt_api.h"
#include "sl_btmesh_api.h"
#include "sl_btmesh_dcd.h"

#include "sl_app_log.h"
#include "sl_app_assert.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT

#include "sl_btmesh_sensor_client_config.h"
#include "sl_btmesh_sensor_client.h"

/***************************************************************************//**
 * @addtogroup SensorClient
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup SensorClient
 * @{
 ******************************************************************************/

/// Number of supported properties
#define PROPERTIES_NUMBER       3
/// Parameter ignored for publishing
#define IGNORED                 0
/// No flags used for message
#define NO_FLAGS                0
/// The size of descriptor is 8 bytes
#define SIZE_OF_DESCRIPTOR      8
/// Size of property ID in bytes
#define PROPERTY_ID_SIZE        2
/// Size of property header in bytes
#define PROPERTY_HEADER_SIZE    3
/// Sensor index value for not registered devices
#define SENSOR_INDEX_NOT_FOUND  0xFF

/// Integer part of temperature
#define INT_TEMP(x) (x / 2)
/// Fractional part of temperature
#define FRAC_TEMP(x) ((x * 5) % 10)

// Address zero is used in sensor client commands to indicate that
// the message should be published
static const uint16_t PUBLISH_ADDRESS = 0x0000;

#if SENSOR_CLIENT_LOGGING == 1
#define log(...) sl_app_log(__VA_ARGS__)
#else
#define log(...)
#endif

typedef struct {
  uint16_t address_table[SENSOR_CLIENT_DISPLAYED_SENSORS];
  uint8_t count;
} mesh_registered_device_properties_address_t;

static bool mesh_address_already_exists(mesh_registered_device_properties_address_t* property,
                                        uint16_t address);
static uint8_t mesh_get_sensor_index(mesh_registered_device_properties_address_t* property,
                                     uint16_t address);

static mesh_registered_device_properties_address_t registered_devices = {
  .count = 0,
};

static mesh_device_properties_t registering_property = DEVICE_PROPERTY_INVALID;

// -----------------------------------------------------------------------------
// Sensor Model Callbacks

SL_WEAK void sl_btmesh_sensor_client_on_discovery_started(uint16_t property_id)
{
  (void)property_id;
}

SL_WEAK void sl_btmesh_sensor_client_on_new_device_found(uint16_t property_id,
                                                         uint16_t address)
{
  (void)property_id;
  (void)address;
}

SL_WEAK void sl_btmesh_sensor_client_on_new_temperature_data(uint8_t sensor_idx,
                                                             uint16_t address,
                                                             sl_btmesh_sensor_client_data_status_t status,
                                                             temperature_8_t temperature)
{

  (void) sensor_idx;

  if (SL_BTMESH_SENSOR_CLIENT_DATA_VALID == status) {
    sl_app_log("Btmesh Sensor Temperature (from 0x%04x): %3d.%1dC\r\n",
               address,
               INT_TEMP(temperature),
               FRAC_TEMP(temperature));
  } else if (SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN == status) {
    sl_app_log("Btmesh Sensor Temperature (from 0x%04x): UNKNOWN\r\n",
               address);
  } else {
    sl_app_log("Btmesh Sensor Temperature (from 0x%04x): NOT AVAILABLE\r\n",
               address);
  }
}

SL_WEAK void sl_btmesh_sensor_client_on_new_humidity_data(uint8_t sensor_idx,
                                                             uint16_t address,
                                                             sl_btmesh_sensor_client_data_status_t status,
                                                             percentage_8_t humidity)
{

  (void) sensor_idx;

  if (SL_BTMESH_SENSOR_CLIENT_DATA_VALID == status) {
    sl_app_log("Btmesh Sensor Humidity (from 0x%04x): %3d.%1d%%\r\n",
               address,
               INT_TEMP(humidity),
               FRAC_TEMP(humidity));
  } else if (SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN == status) {
    sl_app_log("Btmesh Sensor Humidity (from 0x%04x): UNKNOWN\r\n",
               address);
  } else {
    sl_app_log("Btmesh Sensor Humidity (from 0x%04x): NOT AVAILABLE\r\n",
               address);
  }
}

SL_WEAK void sl_btmesh_sensor_client_on_new_acceleration_data(uint8_t sensor_idx,
                                                             uint16_t address,
                                                             sl_btmesh_sensor_client_data_status_t status,
                                                             coefficient_t ax)
{
  (void) sensor_idx;

  if (SL_BTMESH_SENSOR_CLIENT_DATA_VALID == status) {
    sl_app_log("Btmesh Sensor Acceleration (from 0x%04x): %f\r\n",
               address,
               ax);
  } else if (SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN == status) {
    sl_app_log("Btmesh Sensor Acceleration (from 0x%04x): UNKNOWN\r\n",
               address);
  } else {
    sl_app_log("Btmesh Sensor Acceleration (from 0x%04x): NOT AVAILABLE\r\n",
               address);
  }
}

/*******************************************************************************
 * Publishing of sensor client get descriptor request for currently displayed
 * property id. It also resets the registered devices counter.
 ******************************************************************************/
sl_status_t sl_btmesh_sensor_client_update_registered_devices(mesh_device_properties_t property)
{
  registered_devices.count = 0;
  memset(registered_devices.address_table,
         0,
         sizeof(registered_devices.address_table));
  registering_property = property;

  log(SENSOR_CLIENT_LOGGING_START_REGISTERING_DEVICES, property);
  sl_btmesh_sensor_client_on_discovery_started(property);

  sl_btmesh_sensor_client_get_descriptor(PUBLISH_ADDRESS,
                                         BTMESH_SENSOR_CLIENT_MAIN,
                                         IGNORED,
                                         NO_FLAGS,
                                         property);
  return SL_STATUS_OK;
}

/***************************************************************************//**
 * Handling of sensor client descriptor status event.
 *
 * @param[in] evt  Pointer to sensor client descriptor status event.
 ******************************************************************************/
static void handle_sensor_client_descriptor_status(
  sl_btmesh_evt_sensor_client_descriptor_status_t *evt)
{
  sensor_descriptor_t descriptor;
  if (evt->descriptors.len >= SIZE_OF_DESCRIPTOR) {
    mesh_lib_sensor_descriptors_from_buf(&descriptor,
                                         evt->descriptors.data,
                                         SIZE_OF_DESCRIPTOR);
    uint8_t number_of_devices = registered_devices.count;
    if (descriptor.property_id == registering_property
        && number_of_devices < SENSOR_CLIENT_DISPLAYED_SENSORS
        && !mesh_address_already_exists(&registered_devices,
                                        evt->server_address)) {
      registered_devices.address_table[number_of_devices] = evt->server_address;
      registered_devices.count = number_of_devices + 1;
      sl_btmesh_sensor_client_on_new_device_found(descriptor.property_id,
                                                  evt->server_address);
    }
  }
}

/*******************************************************************************
 * Publishing of sensor client get request for currently displayed property id.
 ******************************************************************************/
sl_status_t sl_btmesh_sensor_client_get_sensor_data(mesh_device_properties_t property)
{
  log(SENSOR_CLIENT_LOGGING_GET_DATA_FROM_PROPERTY, property);
  sl_btmesh_sensor_client_get(PUBLISH_ADDRESS,
                              BTMESH_SENSOR_CLIENT_MAIN,
                              IGNORED,
                              NO_FLAGS,
                              property);
  return SL_STATUS_OK;
}

/***************************************************************************//**
 * Handling of sensor client status event.
 *
 * @param[in] evt  Pointer to sensor client status event.
 ******************************************************************************/
static void handle_sensor_client_status(sl_btmesh_evt_sensor_client_status_t *evt)
{
  uint8_t *sensor_data = evt->sensor_data.data;
  uint8_t data_len = evt->sensor_data.len;
  uint8_t pos = 0;

  while (pos < data_len) {
    if (data_len - pos > PROPERTY_ID_SIZE) {
      mesh_device_properties_t property_id = (mesh_device_properties_t)(sensor_data[pos]
                                                                        + (sensor_data[pos + 1] << 8));
      uint8_t property_len = sensor_data[pos + PROPERTY_ID_SIZE];
      uint8_t *property_data = NULL;

      if (mesh_address_already_exists(&registered_devices, evt->server_address)) {
        sl_btmesh_sensor_client_data_status_t status;
        uint16_t address;
        uint8_t sensor_idx;

        if (property_len && (data_len - pos > PROPERTY_HEADER_SIZE)) {
          property_data = &sensor_data[pos + PROPERTY_HEADER_SIZE];
        }

        address = evt->server_address;
        sensor_idx = mesh_get_sensor_index(&registered_devices, address);
        status = SL_BTMESH_SENSOR_CLIENT_DATA_NOT_AVAILABLE;

        switch (property_id) {

          case PRESENT_AMBIENT_TEMPERATURE:
          {
            temperature_8_t temperature = SL_BTMESH_SENSOR_CLIENT_TEMPERATURE_UNKNOWN;

            if (property_len == 1) {
              mesh_device_property_t new_property = mesh_sensor_data_from_buf(PRESENT_AMBIENT_TEMPERATURE,
                                                                              property_data);
              temperature = new_property.temperature_8;

              if (temperature == SL_BTMESH_SENSOR_CLIENT_TEMPERATURE_UNKNOWN) {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN;
              } else {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_VALID;
              }
            } else {
              status = SL_BTMESH_SENSOR_CLIENT_DATA_NOT_AVAILABLE;
            }

            sl_btmesh_sensor_client_on_new_temperature_data(sensor_idx,
                                                            address,
                                                            status,
                                                            temperature);
            break;
          }

          case PRESENT_AMBIENT_HUMIDITY:
          {
            percentage_8_t humidity = SL_BTMESH_SENSOR_CLIENT_HUMIDITY_UNKNOWN;

            if (property_len == 1) {
              mesh_device_property_t new_property = mesh_sensor_data_from_buf(PRESENT_AMBIENT_HUMIDITY,
                                                                              property_data);
              humidity = new_property.percentage;

              if (humidity == SL_BTMESH_SENSOR_CLIENT_HUMIDITY_UNKNOWN) {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN;
              } else {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_VALID;
              }
            } else {
              status = SL_BTMESH_SENSOR_CLIENT_DATA_NOT_AVAILABLE;
            }

            sl_btmesh_sensor_client_on_new_humidity_data(sensor_idx,
                                                            address,
                                                            status,
                                                            humidity);
            break;
          }

          case ACCELERATION_MEASURE:
          {
            coefficient_t ax = SL_BTMESH_SENSOR_CLIENT_ACCELERATION_UNKNOWN;

            if (property_len == 4) {
              mesh_device_property_t new_property = mesh_sensor_data_from_buf(ACCELERATION_MEASURE,
                                                                              property_data);
              ax = new_property.coefficient;

              if (ax == SL_BTMESH_SENSOR_CLIENT_ACCELERATION_UNKNOWN) {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_UNKNOWN;
              } else {
                status = SL_BTMESH_SENSOR_CLIENT_DATA_VALID;
              }
            } else {
              status = SL_BTMESH_SENSOR_CLIENT_DATA_NOT_AVAILABLE;
            }

            sl_btmesh_sensor_client_on_new_acceleration_data(sensor_idx,
                                                            address,
                                                            status,
                                                            ax);
            break;
          }

          default:
            log(SENSOR_CLIENT_LOGGING_UNSUPPORTED_PROPERTY, property_id);
            break;
        }
      }
      pos += PROPERTY_HEADER_SIZE + property_len;
    } else {
      pos = data_len;
    }
  }
}

/*******************************************************************************
 * Handling of mesh sensor client events.
 * It handles:
 *  - sensor_client_descriptor_status
 *  - sensor_client_status
 *
 * @param[in] evt  Pointer to incoming sensor server event.
 ******************************************************************************/
static void handle_sensor_client_events(sl_btmesh_msg_t *evt)
{
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_btmesh_evt_sensor_client_descriptor_status_id:
      handle_sensor_client_descriptor_status(
        &(evt->data.evt_sensor_client_descriptor_status));
      break;

    case sl_btmesh_evt_sensor_client_status_id:
      handle_sensor_client_status(
        &(evt->data.evt_sensor_client_status));
      break;

    default:
      break;
  }
}

/*******************************************************************************
 * Handle Sensor Client events.
 ******************************************************************************/
void sl_btmesh_handle_sensor_client_on_event(sl_btmesh_msg_t *evt)
{
  if (NULL == evt) {
    return;
  }

  // Handle events
  switch (SL_BT_MSG_ID(evt->header)) {
    case sl_btmesh_evt_node_initialized_id:
      if (evt->data.evt_node_initialized.provisioned) {
        sl_btmesh_sensor_client_init();
      }
      break;

    case sl_btmesh_evt_node_provisioned_id:
      sl_btmesh_sensor_client_init();
      break;

    case sl_btmesh_evt_sensor_client_descriptor_status_id:
    case sl_btmesh_evt_sensor_client_status_id:
      handle_sensor_client_events(evt);
      break;

    default:
      break;
  }
}

/***************************************************************************//**
 * Check if the mesh address already exists or not.
 *
 * @param[in] property Pointer to registered devices' properties
 * @param[in] address  Mesh address to check
 *
 * @return             true:  The address exists
 *                     false: The address doesn't exist
 ******************************************************************************/
static bool mesh_address_already_exists(mesh_registered_device_properties_address_t *property,
                                        uint16_t address)
{
  bool address_exists = false;
  if (property != NULL) {
    for (int i = 0; i < SENSOR_CLIENT_DISPLAYED_SENSORS; i++) {
      if (address == property->address_table[i]) {
        address_exists = true;
        break;
      }
    }
  }
  return address_exists;
}

/***************************************************************************//**
 * Gets the sensor index.
 *
 * @param[in] property Pointer to registered devices' properties
 * @param[in] address  Mesh address of the sensor
 *
 * @return             Index of the sensor
 ******************************************************************************/
static uint8_t mesh_get_sensor_index(mesh_registered_device_properties_address_t *property,
                                     uint16_t address)
{
  uint8_t sensor_index = SENSOR_INDEX_NOT_FOUND;
  if (property != NULL) {
    for (int i = 0; i < SENSOR_CLIENT_DISPLAYED_SENSORS; i++) {
      if (address == property->address_table[i]) {
        sensor_index = i;
        break;
      }
    }
  }
  return sensor_index;
}

/** @} (end addtogroup SensorClient) */
