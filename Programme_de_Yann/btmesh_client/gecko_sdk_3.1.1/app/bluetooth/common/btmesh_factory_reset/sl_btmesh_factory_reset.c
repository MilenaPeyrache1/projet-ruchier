/***************************************************************************//**
 * @file  sl_factory_reset.c
 * @brief Factory Reset module
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include "sl_status.h"
#include "sl_bt_api.h"
#include "em_common.h"
#include "sl_app_assert.h"
#include "sl_simple_timer.h"

#ifdef SL_COMPONENT_CATALOG_PRESENT
#include "sl_component_catalog.h"
#endif // SL_COMPONENT_CATALOG_PRESENT

#ifdef SL_CATALOG_CLI_PRESENT
#include "sl_cli.h"
#endif // SL_CATALOG_CLI_PRESENT

#include "sl_btmesh_factory_reset.h"

/***************************************************************************//**
 * @addtogroup FactoryReset
 * @{
 ******************************************************************************/

/// High Priority
#define HIGH_PRIORITY                  0
/// No Timer Options
#define NO_FLAGS                       0
/// Callback has no parameters
#define NO_CALLBACK_DATA               (void *)NULL
/// timeout for factory init
#define FACTORY_RESET_TIMEOUT          2000

/// timer callback
static sl_simple_timer_t factory_reset_timer;

////////////////////////////////////////////////////////////////////////////////
// Provisioning Callbacks                                                     //
////////////////////////////////////////////////////////////////////////////////

static void fatory_reset_timer_cb(sl_simple_timer_t *handle,
                                  void *data);

/*******************************************************************************
 * This function is called to initiate factory reset.
 ******************************************************************************/
void sl_btmesh_initiate_factory_reset()
{
  sl_status_t sc;

  sl_btmesh_factory_reset_on_reset();

  // Perform a system halt until reset
  sc = sl_bt_system_halt(1);
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to halt system\n",
                (int)sc);
  // Perform a factory reset by erasing PS storage. This removes all the keys
  // and other settings that have been configured for this node
  sc = sl_bt_nvm_erase_all();
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start NVM erease \n",
                (int)sc);
  // Reboot after a small delay
  sc = sl_simple_timer_start(&factory_reset_timer,
                             FACTORY_RESET_TIMEOUT,
                             fatory_reset_timer_cb,
                             NO_CALLBACK_DATA,
                             false);
  sl_app_assert(sc == SL_STATUS_OK,
                "[E: 0x%04x] Failed to start Factory reset timer\n",
                (int)sc);
}

/**************************************************************************//**
 * Timer Callbacks
 *****************************************************************************/
static void fatory_reset_timer_cb(sl_simple_timer_t *handle,
                                  void *data)
{
  (void)data;
  (void)handle;
  sl_bt_system_reset(0);
}

/**************************************************************************//**
 * CLI Callbacks
 * @param[in] arguments pointer to CLI arguments
 *****************************************************************************/
#ifdef SL_CATALOG_CLI_PRESENT
void factory_reset_from_cli(sl_cli_command_arg_t *arguments)
{
  (void)arguments;
  sl_btmesh_initiate_factory_reset();
}
#endif // SL_CATALOG_CLI_PRESENT

/**************************************************************************//**
 * Weak implementation of callbacks
 *****************************************************************************/
SL_WEAK void sl_btmesh_factory_reset_on_reset(void)
{
}

/** @} (end addtogroup FactoryReset) */
