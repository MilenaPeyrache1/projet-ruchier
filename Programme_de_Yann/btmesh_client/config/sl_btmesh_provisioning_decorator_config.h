#ifndef SL_BTMESH_PROVISIONING_DECORATOR_CONFIG_H
#define SL_BTMESH_PROVISIONING_DECORATOR_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Provisioning decorator configuration

// <e PROVISIONING_DECORATOR_LOGGING> Enable Logging
// <i> Default: 1
// <i> Enable or disable UART Logging for Provisioning Decorator specific messages for this component.
#define PROVISIONING_DECORATOR_LOGGING   (1)

// </e>

// <o PROVISIONING_DECORATOR_RESTART_TIMER_TIMEOUT> Timeout for system restart after provisioning fails
#define PROVISIONING_DECORATOR_RESTART_TIMER_TIMEOUT          (2000)

// </h>

// <<< end of configuration section >>>

#endif // SL_BTMESH_PROVISIONING_DECORATOR_CONFIG_H
