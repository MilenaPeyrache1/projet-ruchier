#ifndef SL_BTMESH_SENSOR_CLIENT_CONFIG_H
#define SL_BTMESH_SENSOR_CLIENT_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Sensor Client configuration

// <o SENSOR_CLIENT_DISPLAYED_SENSORS> How many sensors can fit on screen
// <i> Default: 5
// <i> Defines the number of sensors which can fit on the LCD screen.
#define SENSOR_CLIENT_DISPLAYED_SENSORS   (5)

// <e SENSOR_CLIENT_LOGGING> Enable Logging
// <i> Default: 1
// <i> Enable / disable UART Logging for Sensor Client model specific messages for this component.
#define SENSOR_CLIENT_LOGGING   (1)

// <s.128 SENSOR_CLIENT_LOGGING_START_REGISTERING_DEVICES> Log text when registering devices .
// <i> Set Log text in case the current sensor property ID is changed
#define SENSOR_CLIENT_LOGGING_START_REGISTERING_DEVICES "Started registering devices for property ID  %4.4x\r\n"

// <s.128 SENSOR_CLIENT_LOGGING_UNSUPPORTED_PROPERTY> Log text in case new Property found.
// <i> Set Log text in case the current sensor property ID is changed
#define SENSOR_CLIENT_LOGGING_UNSUPPORTED_PROPERTY "Unsupported property id %4.4x\r\n"

// <s.128 SENSOR_CLIENT_LOGGING_GET_DATA_FROM_PROPERTY> Log text in case new Property found.
// <i> Set Log text in case the current sensor property ID is changed
#define SENSOR_CLIENT_LOGGING_GET_DATA_FROM_PROPERTY "Get Sensor Data from property ID %4.4x\r\n"

// </h>

// <<< end of configuration section >>>

#endif // SL_BTMESH_SENSOR_CLIENT_CONFIG_H
