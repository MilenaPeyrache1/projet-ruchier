/***************************************************************************//**
 * @file sl_btmesh_event_log_config.h
 * @brief Mesh events logging configuration
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BTMESH_EVENT_LOG_CONFIG_H
#define SL_BTMESH_EVENT_LOG_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <q UNKNOWN_EVENTS_LOG_ENABLE> Enable unknown events logging
// <i> Enables logging of unknown events.
#define UNKNOWN_EVENTS_LOG_ENABLE      0

// <<< end of configuration section >>>

#endif // SL_BTMESH_EVENT_LOG_CONFIG_H
